import subprocess

app_directory = "C:\\Users\\leona\\OneDrive\\Documentos\\DevOps - Somativa 2\\soma_de_notas.py"

result = subprocess.run(["bandit", "-r", app_directory], capture_output=True, text=True)

if result.returncode == 0:
    print("Nenhum problema de segurança encontrado.")
else:
    print("Foram encontrados problemas de segurança:")
    print(result.stdout)
